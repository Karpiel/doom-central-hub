# Doom - Central HUB

![](doc/pics/jkcenhb1.png)
![](doc/pics/jkcenhb2.png)
![](doc/pics/jkcenhb3.png)

## General Description

Doom 2 map in UDMF format.

**Version 0.1**
**Release Date: ??.05.2022**

Central HUB map is ment to be the first map for a bigger project. 

This map is created specifically for GZDoom and Brutal Doom in mind - as always.
                          
And as always it's a tech base style map. This time I wanted to delve a little bit more into ACS scripting and set a common theme for all future maps which means:

- every map will have a boss or many bosses
- ACS scripting will be in use
- custom textures as a separate project (I hope to release it someday)
- medium sized levels
- being careful and paying attention to details should benefit the player and allow him/her to solve some bigger fights in an unusual way
                            
This map is sort of experiment to me to see if it's possible to create more maps sharing the same kind of approach.

Last but not least - again, there's no story behind this level. This part is up to you guys!

Have fun!

## Map Features

- New levels              : 1
- Sounds                  : No
- Music                   : No
- Graphics                : Yes
- Dehacked/BEX Patch      : No
- Demos                   : No
- Other                   : No
- Other files required    : No


*Play Information*

- Game                    : DOOM 2
- Map #                   : MAP1
- Single Player           : Designed for
- Cooperative 2-4 Player  : No
- Deathmatch 2-4 Player   : No
- Other game styles       : None
- Difficulty Settings     : Yes


*Construction*

- Base                    : New from scratch
- Build Time              : Around 2 months 
- Editor(s) used          : Ultimate Doom Builder
- Known Bugs              : Unknown
- May Not Run With        : Unknown
- Tested With             : GZDoom 4.4.2, Zandronum 3.0, Brutal Doom 21 RC1 FIX, Super Charge 2.8

*Special thanks*

To be written

## Links

[Author Fan Page](https://www.facebook.com/World-Reloaded-107897790984291)


